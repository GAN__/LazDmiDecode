unit globals;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  DMI_Q=43;
  DMI_CAT_Q=10;

  DMITYPES:array[0..DMI_Q-1] of string=(
            'BIOS',        //  0
            'System',      //  1
            'Baseboard',   //  2
            'Chassis',     //  3
            'Processor',   //  4
            'Memory Controller', //  5
            'Memory Module',     //  6
            'Cache',             //  7
            'Port Connector',    //  8
            'System Slots',      //  9
            'On Board Devices',  // 10
            'OEM Strings',       // 11
            'System Configuration Options', // 12
            'BIOS Language',                // 13
            'Group Associations',           // 14
            'System Event Log',             // 15
            'Physical Memory Array',        // 16
            'Memory Device',                // 17
            '32-bit Memory Error',          // 18
            'Memory Array Mapped Address',  // 19
            'Memory Device Mapped Address', // 20
            'Built-in Pointing Device',     // 21
            'Portable Battery',             // 22
            'System Reset',                 // 23
            'Hardware Security',            // 24
            'System Power Controls',        // 25
            'Voltage Probe',                // 26
            'Cooling Device',               // 27
            'Temperature Probe',            // 28
            'Electrical Current Probe',     // 29
            'Out-of-band Remote Access',    // 30
            'Boot Integrity Services',      // 31
            'System Boot',                  // 32
            '64-bit Memory Error',          // 33
            'Management Device',            // 34
            'Management Device Component',  // 35
            'Management Device Threshold Data', // 36
            'Memory Channel',                   // 37
            'IPMI Device',                      // 38
            'Power Supply',                     // 39
            'Additional Information',           // 40
            'Onboard Devices Extended Information', // 41
            'Management Controller Host Interface'); //42

  DMICATS:array[0..DMI_CAT_Q-1] of string=(
            'BIOS',
            'System',
            'Baseboard',
            'Chassis',
            'Processor',
            'Memory',
            'Cache',
            'Port Connector',
            'System Slots',
            'Others');

type
  Category=0..42;

  Categories=set of Category;

function GetCat(aType:Integer):Integer;

var
  sBIOS:Categories;
  sSystem:Categories;
  sBaseBoard:Categories;
  sChassis:Categories;
  sProcessor:Categories;
  sMemory:Categories;
  sCache:Categories;
  sConnector:Categories;
  sSlot:Categories;
  sOthers:Categories;
  sAllCats:Categories;

implementation

function GetCat(aType: Integer): Integer;
begin
  if aType in sBIOS      then Exit(0);
  if aType in sSystem    then Exit(1);
  if aType in sBaseBoard then Exit(2);
  if aType in sChassis   then Exit(3);
  if aType in sProcessor then Exit(4);
  if aType in sMemory    then Exit(5);
  if aType in sCache     then Exit(6);
  if aType in sConnector then Exit(7);
  if aType in sSlot      then Exit(8);
  if aType in sOthers    then Exit(9);
end;



initialization

  sBIOS:=[0,13];
  sSystem:=[1,12,15,23,32];
  sBaseBoard:=[2,10,41];
  sChassis:=[3];
  sProcessor:=[4];
  sMemory:=[5,6,16,17];
  sCache:=[7];
  sConnector:=[8];
  sSlot:=[9];
  sAllCats:=[0..42];
  sOthers:=sAllCats-sBIOS-sSystem-sBaseBoard-sChassis-sProcessor-sMemory-sConnector-sSlot;

finalization;




end.

{
 bios        0, 13
 system      1, 12, 15, 23, 32
 baseboard   2, 10, 41
 chassis     3
 processor   4
 memory      5, 6, 16, 17
 cache       7
 connector   8
 slot        9
}






