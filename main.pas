unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons, process, uhelp,
  ComCtrls, globals;

type

  { TFmain }

  TFmain = class(TForm)
    BDMI: TBitBtn;
    BHelp: TBitBtn;
    BExit: TBitBtn;
    ImageList1: TImageList;
    lName: TLabel;
    Memo1: TMemo;
    tv: TTreeView;
    procedure BDMIClick(Sender: TObject);
    procedure BExitClick(Sender: TObject);
    procedure BHelpClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure tvClick(Sender: TObject);
  private
    dmiList:TStringList;
    function LoadList:Boolean;
    procedure CleanList;
    procedure LoadRoots;
    procedure LoadNodes;
    procedure LoadIcons;
  public

  end;

  PRec=^TRec;

  TRec=record
    id:word;
    cat:word;
    info:string;
  end;

var
  Fmain: TFmain;

implementation

{$R *.lfm}

{ TFmain }

procedure TFmain.BDMIClick(Sender: TObject);    // sudo dmidecode -q
begin
  if not(LoadList) then Exit;
  BDMI.Enabled:=False;
  CleanList;
  LoadRoots;
  LoadIcons;
  LoadNodes;
end;

function TFmain.LoadList: Boolean;
var
  proc:TProcess;
begin
  if Assigned(dmiList) then dmiList.Free;
  dmiList:=TStringList.Create;
  proc:=TProcess.Create(nil);
  proc.CommandLine:='pkexec dmidecode';
  proc.Options:=proc.Options+[poWaitOnExit, poUsePipes];
  proc.Execute;
  dmiList.LoadFromStream(proc.Output);
  proc.Free;
  Result:=dmiList.Count>0;
end;

procedure TFmain.CleanList;
var
  FounfHandle:Boolean=False;
begin
  while not(FounfHandle) do
    begin
      FounfHandle:=LeftStr(dmiList[0],6)='Handle';
      if not(FounfHandle) then dmiList.Delete(0);
    end;
end;

procedure TFmain.BHelpClick(Sender: TObject);
begin
  FHelp.ShowModal;
end;

procedure TFmain.LoadRoots;
var
  i:Word;
begin
  for i:= Low(DMICATS) to High(DMICATS) do tv.Items.Add(nil,DMICATS[i]);
end;

procedure TFmain.LoadNodes;
var
  i,j,level:Integer;
  nPosType, ntype:integer;
  aux, recinfo:String;
  node:PRec;
  bEnd, EndRec:Boolean;
begin
  bEnd:=False;
  i:=0;
  while not(bEnd) do
    begin
      if LeftStr(dmiList[i],6)='Handle' then
        begin
          nPosType:=Pos('type',dmiList[i])+5;
          aux:=Copy(dmiList[i],nPosType,2); // 0,   41
          if RightStr(aux,1)=',' then Delete(aux,2,1);
          if (aux='12') and (LeftStr(dmiList[i+1],12)='End Of Table') then
            bEnd:=True
          else
            begin
              ntype:=StrToInt(aux);
              level:=GetCat(ntype);
              Inc(i);
              j:=i+1;
              recinfo:='';
              EndRec:=False;
              New(node);
              node^.id:=0;
              node^.cat:=ntype;
              repeat
                if ((dmiList[j]<>'') and (LeftStr(dmiList[j],6)<>'Handle')) then
                begin
                  recinfo:=recinfo+dmiList[j]+LineEnding;
                end;
                EndRec:=(LeftStr(dmiList[j],6)='Handle');
                Inc(i);
                Inc(j);
              until EndRec;
              node^.info:=recinfo;
              tv.Items.AddChildObject(tv.Items.FindTopLvlNode(DMICATS[level]), DMITYPES[ntype],node);
             end;
        end;
     end;
end;

procedure TFmain.LoadIcons;
var
  i:Integer;
begin
  for i:=0 to tv.Items.Count-1 do
    begin
      tv.Items[i].ImageIndex:=i;
      tv.Items[i].SelectedIndex:=i;
    end;
end;

procedure TFmain.tvClick(Sender: TObject);
begin
  if tv.Selected=nil then Exit;
  if tv.Items.Count<1 then Exit;
  if (tv.Selected.Data<>nil) then
  begin
    if tv.Selected.Level=0 then exit;
    lName.Caption:=tv.Selected.Text;
    Memo1.Text:=PRec(tv.Selected.Data)^.info;
  end;
end;

procedure TFmain.BExitClick(Sender: TObject);
begin
  Close;
end;

procedure TFmain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction:=caFree;
end;


end.

{
Followings are the  DMI types that are used on dmidecode command

       Type   Information
       --------------------------------------------
          0   BIOS
          1   System
          2   Baseboard
          3   Chassis
          4   Processor
          5   Memory Controller
          6   Memory Module
          7   Cache
          8   Port Connector
          9   System Slots
         10   On Board Devices
         11   OEM Strings
         12   System Configuration Options
         13   BIOS Language
         14   Group Associations
         15   System Event Log
         16   Physical Memory Array
         17   Memory Device
         18   32-bit Memory Error
         19   Memory Array Mapped Address
         20   Memory Device Mapped Address
         21   Built-in Pointing Device
         22   Portable Battery
         23   System Reset
         24   Hardware Security
         25   System Power Controls
         26   Voltage Probe
         27   Cooling Device
         28   Temperature Probe
         29   Electrical Current Probe
         30   Out-of-band Remote Access
         31   Boot Integrity Services
         32   System Boot
         33   64-bit Memory Error
         34   Management Device
         35   Management Device Component
         36   Management Device Threshold Data
         37   Memory Channel
         38   IPMI Device
         39   Power Supply
         40   Additional Information
         41   Onboard Devices Extended Information
         42   Management Controller Host Interface

       Keyword     Types
       ------------------------------
       bios        0, 13
       system      1, 12, 15, 23, 32
       baseboard   2, 10, 41
       chassis     3
       processor   4
       memory      5, 6, 16, 17
       cache       7
       connector   8
       slot        9

}
