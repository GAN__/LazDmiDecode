unit uhelp;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls, LCLIntf,
  Buttons;

type

  { TFHelp }

  TFHelp = class(TForm)
    BClose: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    lLazarus: TLabel;
    lFPC: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure BCloseClick(Sender: TObject);
    procedure lFPCClick(Sender: TObject);
    procedure lLazarusClick(Sender: TObject);
  private

  public

  end;

var
  FHelp: TFHelp;

implementation

{$R *.lfm}

{ TFHelp }

procedure TFHelp.BCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFHelp.lFPCClick(Sender: TObject);
begin
  OpenURL('https://www.freepascal.org/');
end;

procedure TFHelp.lLazarusClick(Sender: TObject);
begin
  OpenURL('https://www.lazarus-ide.org/');
end;


end.

